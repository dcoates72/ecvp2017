proj=$(notdir $(CURDIR) )

all:
	pdflatex $(proj).tex
	#bibtex $(proj)
	#pdflatex $(proj).tex
	#pdflatex $(proj).tex
	mkdir -p coates
	cp -av ecvp2017.pdf coates
	cp -av movie.mp4 coates


clean:
	rm -f $(proj).vrb $(proj).snm $(proj).out $(proj).aux $(proj).toc $(proj).nav $(proj).log $(proj).bbl $(proj).blg

view:
	evince $(proj).pdf 2>/dev/null &

pull:
	git checkout -- $(proj).pdf
	git pull
 
handout:
	gs -q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -sOutputFile=$(proj)-low.pdf $(proj).pdf

rename:
	#cp -a ../talk_template $(proj)
	mv talk_template.tex $(proj).tex
	echo "Don't forget to change git!"
	rm -fr .git
	git init
	git remote add origin git@bitbucket.org:dcoates72/$(proj).git
	git add *.sty Makefile $(proj).tex .gitignore


# 907,46  45,589

examples_crop:
	rm -f img/gal5.png
	convert img/gal5.pdf -crop 667x410+0+26 img/gal5.png
	convert img/gal5_color.pdf -crop 667x410+0+26 img/gal5_color.png
